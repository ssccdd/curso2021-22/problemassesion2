﻿
﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 1
Problemas propuestos para la Sesión 2 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2021-22.

Los objetivos de la práctica son:
- Definir adecuadamente las clase que representa a lo que se ejcutará en el hilo. Programando adecuadamente la excepción de interrupción de su ejecución.
- Modificar el método main(..) de la aplicación Java para una prueba correcta de las clases previamente definidas.
	- Crear los objetos que representan lo que deberá ejecutarse en los hilos.
	- Crear los objetos de los hilos de ejecución. Asociando correctamente lo que deberá ejercutarse.
	- La interrupción de los hilos cuando sea solicitado.

Los ejercicios son diferentes para cada grupo:
- [Grupo 1](https://gitlab.com/ssccdd/curso2021-22/problemassesion2/-/blob/master/README.md#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2021-22/problemassesion2/-/blob/master/README.md#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2021-22/problemassesion2/-/blob/master/README.md#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2021-22/problemassesion2/-/blob/master/README.md#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2021-22/problemassesion2/-/blob/master/README.md#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2021-22/problemassesion2/-/blob/master/README.md#grupo-6)

### Grupo 1
Se deberán completar una serie de pedidos para unos clientes que tendrán una disponibilidad de recursos limitada. Para ello se deberán realizar las siguientes tareas:  

-   Añadir todas las constantes necesarias en la interface  **Constantes**  para la resolución del ejercicio.
-   **GestorPedidos**:  Esta tarea simulará la preparación de pedidos para unas personas compartidas entre los gestores.
    -   _Atributos_
    
	    -   Nombre del gestor.
	    -   Lista compartida entre todos los gestores donde estarán disponibles las personas a las que se le prepara el pedido.
	    -   Lista compartida entre todos los gestores donde se almacenará la lista de los pedidos que ha preparado en su ejecución. Cada gestor tendrá su espacio donde almacenar sus pedidos.
    
    -   _Ejecución de la tarea_
        -   Se genera un producto de forma aleatoria.
        -   Se asignará el producto a uno de los pedidos disponibles. Si no hay pedido disponible o no se ha podido asignar el producto se obtendrá una persona de la lista compartida para crear un nuevo pedido donde incluir el producto.
        -   Si no hay personas disponibles para crear un nuevo pedido finalizará la ejecución de la tarea.
        -   Asignar el producto al pedido se simulará con un tiempo de operación que estará comprendido entre 1 y 3 segundos.
        -   Se repetirá el proceso hasta la finalización de la ejecución de la tarea o si se ha solicitado su interrupción.
        -   Antes de finalizar se almacenarán los pedidos completados hasta ese momento en la lista compartida con los gestores.
        -   La tarea deberá indicar si ha finalizado su ejecución o si ha sido solicitada la interrupción en su finalización.
-   **HiloPrincipal**:

	-   Crean tres  **GestorPedidos**  y les asocia los hilos para su ejecución. Se deben crear las variables compartidas necesarias para estos gestores.
	-   El hilo principal crea 30 personas con recursos variables entre 40 y 60. El tiempo necesario para la creación de una persona será de 1 segundo.
	-   Una vez que se ha finalizado la creación de las personas esperará por 30 segundos antes de solicitar la interrupción de los gestores esperando a su finalización.
    
	-   Antes de finalizar presentar la lista de pedidos asociada a cada gestor.

### Grupo 2

En un sistema informático los diferentes procesos que se crean deberán almacenarse en memoria para su correcto funcionamiento.

Para la realización del ejercicio propuesto se deben crear las siguientes clases:

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   **GestorProcesos**: Esta tarea simulará una gestión básica de procesos atendiendo a su uso de memoria.
    -   _Atributos_
        -   Identificador del gestor.
        -   Lista de procesos que será compartida entre todos los gestores.
        -   Lista donde almacenar el resultado de la ejecución del gestor. Cada gestor tendrá su espacio en esta lista.
    -   _Ejecución_
        -   Creará un  **RegistroMemoria** con un número de páginas aleatorio entre 20 y 40.
        -   Repetir
            -   Obtener un proceso de la lista compartida.
            -   Asignarlo al registro de memoria.
            -   Se simulará un tiempo de ejecución para este procedimiento que estará comprendido entre 2 y 4 segundos.
            -   Si no se puede añadir al registro de memoria se volverá a incluir en la lista compartida de procesos.
            -   Si no hay proceso indicará el fin de la tarea. También se debe implementar la interrupción de la tarea.
        -   Antes de finalizar deberá almacenar en la lista de resultados el registro de memoria que ha creado el gestor.
        -   La tarea deberá indicar si ha finalizado su ejecución o si ha sido solicitada la interrupción en su finalización.
    -   **Hilo Principal**:
        -   Se crearán 5 gestores y las variables compartidas necesarias. Se asignarán los hilos necesarios y se ejecutarán.
        -   Se crearán entre 50 y 80 procesos y se añaden uno a uno a la lista de procesos compartida entre los gestores.
        -   El tiempo necesario para añadir un proceso a la lista será de 1 segundo.
        -   Se suspenderá la ejecución por un periodo de 30 segundos.
        -   Se solicitará la interrupción de los gestores pasado ese tiempo y esperará a la finalización de todos ellos.
        -   Antes de finalizar se presentará el resultado de la ejecución de cada uno de los gestores.

### Grupo 3

Tenemos un proveedor que deberá componer diferentes ordenadores que tendrán a su disposición una serie de componentes.

Para la realización del ejercicio propuesto se deben crear las siguientes clases:

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   **Proveedor**: Será una tarea que simula el trabajo de un operador que estará completando una serie de ordenadores según una lista de componentes.
    -   _Atributos_
        -   Nombre del proveedor.
        -   Lista compartida de componentes entre todos los proveedores.
        -   Lista de resultados donde se almacenará, para cada proveedor, la lista de ordenadores que ha realizado durante su ejecución.
    -   _Ejecución_
        -   Se obtendrá un componente de la lista compartida entre los gestores.
        -   Se asignará el componente a uno de los ordenadores disponibles. Si no se ha podido asignar a ningún ordenador se creará uno nuevo y se le asignará el componente.
        -   Si no se ha conseguido un componente de la lista tras 3 intentos se dará por finalizada la ejecución del proveedor.
        -   Asignar el componente a un ordenador se simulará con un tiempo de operación que estará comprendido entre 1 y 3 segundos.
        -   Se repetirá el proceso hasta la finalización de la ejecución de la tarea o si se ha solicitado su interrupción.
        -   Antes de finalizar se almacenarán los pedidos completados hasta ese momento en la lista compartida con los gestores.
        -   La tarea deberá indicar si ha finalizado su ejecución o si ha sido solicitada la interrupción en su finalización.
    -   **HiloPrincipal**:
        -   Crean cuatro  **Proveedor** y les asocia los hilos para su ejecución. Se deben crear las variables compartidas necesarias para estos proveedores.
        -   El hilo principal crea entre 30 y 60 componentes. El tiempo necesario para la creación de un componente será de 1 ó 2 segundos.
        -   Una vez que se ha finalizado la creación de los componentes esperará por 30 segundos antes de solicitar la interrupción de los proveedores esperando a su finalización.
        -   Antes de finalizar se debe presentar la lista de los ordenadores que cada uno de los proveedores ha realizado durante su ejecución.

### Grupo 4

En el ejercicio se pretende simular la creación de diferentes planes de estudios en diferentes plataformas, a petición del hilo principal. El hilo principal esperára un tiempo y posteriormente interrumpirá el proceso de organización. Para la solución se tiene que utilizar los siguientes elementos ya programados:

 - `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
 - `Recurso`: Clase que representa a un recurso de aprendizaje de tipo `TipoRecurso`.
 - `PlanEstudios`: Representa a un plan de estudios. Como máximo puede tener un recurso de blog y foro, no puede tener mas de un foro, ni mas de un blog, pero si un foro y un blog a la vez. Y la suma de créditos de las asignaturas no puede ser mayor a 8.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `PlataformaAprendizaje`: Representa un sistema de gestión para diferentes entidades educativas. Que procesa los diferentes planes de estudios y recursos de aprendizaje. Tendrá las siguientes variables de instancia:
	- Un identificador.
	- Una serie de planes de estudios para completar con recursos.
	- Una lista de recursos de aprendizaje para asignar en los planes.
	- Una lista en común con el resto de plataformas para almacenar los recursos descartados.
	- Los pasos que debe realizar son:
		- Organizar los recursos:
			- Procesar un recurso: inserta el recurso en el primer plan de estudios que lo pueda admitir, si no se puede insertar en ninguno se guarda en la lista común de descartados.
			- Se simula el tiempo de trabajo de entre 1 a 3 segundos, ambos incluidos
			- El recurso se considera procesado se haya podido insertar o no, excepto en el caso de que se interrumpa, los no procesados por falta de tiempo no cuentan como procesados ni se guardan en la lista de descartados.
			- Una vez interrumpido el proceso se ignorarán los recursos que no sean de tipo `ASIGNATURA`, todas las asignaturas deben procesarse.
			- A la finalización del trabajo, o interrupción, se debe presentar el trabajo realizado por la plataforma, debe incluir como mínimo el porcentaje de recursos procesados, la información de los planes de estudiso y debe quedar claro si el proceso ha sido cancelado/interrumpido. Las salidas por consola de los diferentes procesos no deben intercalarse.
- `HiloPrincipal`: Realizará los siguientes pasos:
	- Crear una estructura única para los recursos que no se han podido asignar a ningún plan de estudios.
	- Crear 7 plataformas:
		- Asignar a cada plataforma 10 planes de estudios y 30 recursos de aprendizaje y la estructura común de descartados.
		- Asociar un objeto `Thread` a cada plataforma para su ejecución.
	- Ejecutar los hilos.
	- Suspender el hilo principal durante 30 segundos.
	- Pasados los 30 segundos, solicitar la interrupción de todos los trabajos.
	- Esperar a que todos los procesos terminen.
	- Imprimir los datos de los recursos descartados.

### Grupo 5
En el ejercicio se pretende simular la asignación de coches a las reservas pendientes por un gestor, a petición del hilo principal. El hilo principal esperára un tiempo y posteriormente interrumpirá el proceso de organización. Para la solución se tiene que utilizar los siguientes elementos ya programados:

 - `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
 - `Coche`: Identificará a un coche disponible por medio de un número de identificación. También tendrá asociado un TipoReserva que identifica la gama del coche.
 - `Reserva`: Identificará a una reserva de alquiler por medio de un número de identificación y contará con una propiedad que indique la gama de coche reservada y cuantos coches son necesarios. Solo se le podrán asignar coches que coincidan con la gama elegida y sin sobrepasar los coches solicitados.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `Gestor`: Representa un trabajador encargado de organizar las diferentes reservas. Tendrá las siguientes variables de instancia:
	- Un identificador.
	- Una serie de reservas para completar con coches.
	- Una lista de coches para asignar.
	- Una lista en común con el resto de gestores para almacenar los coches descartados.
	- Los pasos que debe realizar son:
		- Organizar los coches en reservas:
			- Procesar un coche: inserta el coche en una reserva, si no se puede insertar en ninguna se guarda en la lista común de descartados. El tipo de distribución se mantiene, se intentará asignar coches de manera equitativa.
			- Se simula el tiempo de trabajo de entre 2 a 5 segundos, ambos incluidos
			- El coche se considera procesado se haya podido insertar o no, excepto en el caso de que se interrumpa, los no procesados por falta de tiempo no cuentan como procesados ni se guardan en la lista de descartados.
			- Una vez interrumpido el proceso se seguirán procesando solo aquellos coches de la gama `PREMIUM`, el resto se ignorará.
			- A la finalización del trabajo, o interrupción, se debe presentar el trabajo realizado por el gestor, debe incluir como mínimo el porcentaje de coches procesados, la información de las reservas y debe quedar claro si el proceso ha sido cancelado/interrumpido. Las salidas por consola de los diferentes procesos no deben intercalarse.
- `HiloPrincipal`: Realizará los siguientes pasos:
	- Crear una estructura única para los coches que no se han podido asignar a ningún plan de estudios.
	- Crear 5 gestores:
		- Asignar a cada gestor 12 reservas, entre 10 y 25 coches y la estructura común de descartados.
		- Asociar un objeto `Thread` a cada gestor para su ejecución.
	- Ejecutar los hilos.
	- Suspender el hilo principal durante 30 segundos.
	- Pasados los 30 segundos, solicitar la interrupción de todos los trabajos.
	- Esperar a que todos los procesos terminen.
	- Imprimir los datos de los coches descartados.

### Grupo 6
En el ejercicio se pretende simular la asignación de jugadores a los equipos por un organizador, a petición del hilo principal. El hilo principal esperára un tiempo y posteriormente interrumpirá el proceso de organización. Para la solución se tiene que utilizar los siguientes elementos ya programados:

 - `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
 - `Jugador`: Identificará a un jugador por medio de un número de identificación. También tendrá asociado un Rol para su estilo de juego.
- `Equipo`: Identificará a un equipo por medio de un número de identificación. Como máximo puede tener dos jugadores con el mismo rol y como mínimo dispondrá de un atacante. Y se considerará equipo completo si tiene cuatro jugadores.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `Organizador`: Representa un trabajador encargado de organizar las diferentes competiciones. Tendrá las siguientes variables de instancia:
	- Un identificador.
	- Una serie de equipos para completar con jugadores.
	- Una lista de jugadores para asignar.
	- Una lista en común con el resto de organizadores para almacenar los jugadores descartados.
	- Los pasos que debe realizar son:
		- Organizar los jugadores en equipos:
			- Procesar un jugador: inserta al jugador en el primer equipo que lo pueda admitir, si no se puede insertar en ninguno se guarda en la lista común de descartados.
			- Se simula el tiempo de trabajo de entre 0 a 3 segundos, ambos incluidos
			- El jugador se considera procesado se haya podido insertar o no, excepto en el caso de que se interrumpa, los no procesados por falta de tiempo no cuentan como procesados ni se guardan en la lista de descartados.
			- Una vez interrumpido el proceso se seguirán procesando jugadores hasta que se hayan completado 6 equipos o no queden jugadores disponibles.
			- A la finalización del trabajo, o interrupción, se debe presentar el trabajo realizado por el organizador, debe incluir como mínimo el porcentaje de jugadores procesados, cuantos equipos han sido completados, la información de los equipos y debe quedar claro si el proceso ha sido cancelado/interrumpido. Las salidas por consola de los diferentes procesos no deben intercalarse.
- `HiloPrincipal`: Realizará los siguientes pasos:
	- Crear una estructura única para los juagdores que no se han podido asignar a ningún equipo.
	- Crear 4 organizadores:
		- Asignar a cada organizador 12 equipos, 40 jugadores y la estructura común de descartados.
		- Asociar un objeto `Thread` a cada organizador para su ejecución.
	- Ejecutar los hilos.
	- Suspender el hilo principal durante 30 segundos.
	- Pasados los 30 segundos, solicitar la interrupción de todos los trabajos.
	- Esperar a que todos los procesos terminen.
	- Imprimir los datos de los jugadores descartados.
