/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.FIN_EJECUCION;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.PRIMERO;
import es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.TipoProducto;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorPedidos implements Runnable {
    private final String iD;
    private final ArrayList<Persona> listaPersonas;
    private final ArrayList<Pedido> resultadoEjecucion;

    public GestorPedidos(String iD, ArrayList<Persona> listaPersonas, ArrayList<Pedido> resultadoEjecucion) {
        this.iD = iD;
        this.listaPersonas = listaPersonas;
        this.resultadoEjecucion = resultadoEjecucion;
    }
    
    @Override
    public void run() {
        boolean finEjecucion;
        TipoProducto producto;
        
        System.out.println("El HILO(" + iD + ") comienza su ejecución");
        finEjecucion = !FIN_EJECUCION;
        
        try {
            // Hasta su finalización o su interrupción
            do {
                producto = TipoProducto.getTipoProducto();
                if( !asignarProducto(producto) )
                    finEjecucion = nuevoPedido(producto);
                
            } while(!finEjecucion);
            
            System.out.println("El HILO(" + iD + ") ha finalizado su ejecución");
        } catch(InterruptedException ex) {
            System.out.println("El HILO(" + iD + ") ha sido INTERRUMPIDO");
        }
    }

    public String getiD() {
        return iD;
    }

    /**
     * Asignamos el producto al primer pedido disponible y devolvemos el resultado 
     * de esta operación. 
     * @param producto
     * @return resultado de la acción si ha podido realizar o no
     * @throws InterruptedException 
     */
    private boolean asignarProducto(TipoProducto producto) throws InterruptedException {
        int tiempoProcesado;
        boolean asignado = !ASIGNADO;
        Iterator it;
        
        // Comprobamos si han solicitado la interrupción
        if( Thread.interrupted() )
            throw new InterruptedException();
        
        // Asignamos el producto al primer pedido posible
        it = resultadoEjecucion.iterator();
        while( it.hasNext() && !asignado ) { 
            Pedido pedido = (Pedido) it.next();
            if( pedido.addProducto(producto) ) 
                asignado = ASIGNADO;    
        }
        
        // Se simula un tiempo de procesamiento
        tiempoProcesado = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
        TimeUnit.SECONDS.sleep(tiempoProcesado);
        
        return asignado;
    }
    
    /**
     * Se obtiene una persona de la lista compartida entre los gestores para crear
     * un nuevo pedido donde asignar el producto. Si no ha persona se indica que ha
     * finalizado la ejecución del gestor.
     * @param producto
     * @return si ha finalizado el gestor o no.
     */
    private boolean nuevoPedido(TipoProducto producto) {
        boolean finEjecucion = !FIN_EJECUCION;
        Optional<Persona> persona;
        Pedido pedido;
        
        try {
            persona = Optional.ofNullable(listaPersonas.remove(PRIMERO));
        } catch(IndexOutOfBoundsException ex) {
            // No hay personas en la lista
            persona = Optional.empty();
        }
        
        if( persona.isPresent() ) {
            pedido = new Pedido("Pedido-" + resultadoEjecucion.size(), persona.get());
            pedido.addProducto(producto);
            resultadoEjecucion.add(pedido);
        
            System.out.println("El HILO(" + iD + ") ha creado un pedido para " + persona.get());
        } else
            finEjecucion = FIN_EJECUCION;
        
        return finEjecucion;
    }
}
