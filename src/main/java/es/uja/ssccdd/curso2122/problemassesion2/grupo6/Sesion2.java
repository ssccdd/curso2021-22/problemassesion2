/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo6;

import es.uja.ssccdd.curso2122.problemassesion2.grupo6.Utils.RolJugador;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo6.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion2 {

    public static void main(String[] args) {
        // Variables aplicación
        int idEquipos = 0;
        int idJugadores = 0;
        int idOrganizadores = 0;

        ArrayList<Thread> hilosOrganizadores = new ArrayList<>();
        ArrayList<Jugador> jugadoresPerdidos = new ArrayList<>();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < ORGANIZADORES_A_GENERAR; i++) {

            // Inicializamos los equipos
            ArrayList<Equipo> listaEquipos = new ArrayList<>();
            for (int j = 0; j < EQUIPOS_A_GENERAR; j++) {
                listaEquipos.add(new Equipo(idEquipos++));
            }

            // Inicializamos los jugadores
            ArrayList<Jugador> listaJugadores = new ArrayList<>();
            for (int j = 0; j < JUGADORES_A_GENERAR; j++) {
                int aleatorioJugador = random.nextInt(VALOR_GENERACION);
                listaJugadores.add(new Jugador(idJugadores++, RolJugador.getRolJugador(aleatorioJugador)));
            }

            // Inicializamos las plataformas
            Organizador plataforma = new Organizador(idOrganizadores++, listaEquipos, listaJugadores, jugadoresPerdidos);
            Thread thread = new Thread(plataforma);
            thread.start();
            hilosOrganizadores.add(thread);

        }

        System.out.println("HILO-Principal Espera a la finalización de los organizadores.");

        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de los organizadores.");

        for (int i = 0; i < ORGANIZADORES_A_GENERAR; i++) {
            hilosOrganizadores.get(i).interrupt();
        }

        System.out.println("HILO-Principal Espera a los organizadores.");

        for (int i = 0; i < ORGANIZADORES_A_GENERAR; i++) {
            try {
                hilosOrganizadores.get(i).join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\nLos siguientes " + jugadoresPerdidos.size() + " jugadores no han podido ser asignados:");
        for (Jugador p : jugadoresPerdidos) {
            System.out.println("\t" + p);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }
}
