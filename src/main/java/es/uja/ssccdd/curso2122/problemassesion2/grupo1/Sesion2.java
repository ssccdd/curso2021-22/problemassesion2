/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.INCREMENTO_RECURSOS;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.MIN_RECURSOS;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.NUM_PERSONAS;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.TIEMPO_FINALIZACION;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.TIEMPO_LLEGADA_PERSONA;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author UJA
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        Thread[] listaHilos;
        GestorPedidos gestor;
        ArrayList<Persona> listaPersonas;
        ArrayList<Pedido>[] listaResultados;
        Persona persona;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        listaPersonas = new ArrayList();
        listaResultados = new ArrayList[NUM_GESTORES];
        listaHilos = new Thread[NUM_GESTORES];
        for(int i = 0; i < NUM_GESTORES; i++) {
            listaResultados[i] = new ArrayList();
            gestor = new GestorPedidos("Gestor-" + i, listaPersonas, listaResultados[i]);
            listaHilos[i] = new Thread(gestor);
        }
        
        // Cuerpo de ejecución del hilo principal
        for(Thread hilo : listaHilos)
            hilo.start();
        
        System.out.println("Hilo(PRINCIPAL) comienza la llegada de las personas");
        try {
            for(int i = 0; i < NUM_PERSONAS; i++) {
                persona = new Persona("Persona-" + i, MIN_RECURSOS + 
                                                  aleatorio.nextInt(INCREMENTO_RECURSOS));
                listaPersonas.add(persona);
                TimeUnit.SECONDS.sleep(TIEMPO_LLEGADA_PERSONA);
            }
            
            // Espera antes de enviar la señal de interrupción y espera la finalización
            System.out.println("Hilo(PRINCIPAL) a la espera antes de enviar la señal de INTERRUPCION");
            TimeUnit.SECONDS.sleep(TIEMPO_FINALIZACION);
        } catch(IndexOutOfBoundsException ex) {
            System.out.println("Hilo(PRINCIPAL) ERROR DE CONCURRENCIA en la lista de personas");
        }
        
        System.out.println("Hilo(PRINCIPAL) solicita la INTERRUPCION de los gestores y espera a su finalización");
        for(Thread hilo : listaHilos)
            hilo.interrupt();
        
        for(Thread hilo : listaHilos)
            hilo.join();
        
        // Presentar resultados
        for(int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("_______ Resultados Gestor " + i + " _____________");
            for(Pedido pedido : listaResultados[i])
                System.out.println(pedido);
            System.out.println("_________________________________________________");
        }
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
