/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.INCREMENTO_PROCESOS;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.MIN_PAGINAS;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.MIN_PROCESOS;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.TIEMPO_CREAR_PROCESO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.TIEMPO_FINALIZACION;
import es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.VARIACION_PAGINAS;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author UJA
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        Thread[] listaHilos;
        GestorProcesos gestor;
        ArrayList<Proceso> listaProcesos;
        RegistroMemoria[] listaResultados;
        Proceso proceso;
        int paginas;
        int procesos;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        listaProcesos = new ArrayList();
        listaResultados = new RegistroMemoria[NUM_GESTORES];
        listaHilos = new Thread[NUM_GESTORES];
        for(int i = 0; i < NUM_GESTORES; i++) {
            paginas = MIN_PAGINAS + aleatorio.nextInt(VARIACION_PAGINAS);
            listaResultados[i] = new RegistroMemoria("Registro-" + i, paginas);
            gestor = new GestorProcesos("Gestor-" + i, listaProcesos, listaResultados[i]);
            listaHilos[i] = new Thread(gestor);
        }
        
        // Cuerpo de ejecución del hilo principal
        for(Thread hilo : listaHilos)
            hilo.start();
        
        System.out.println("Hilo(PRINCIPAL) comienza la creación de procesos");
        try {
            procesos = MIN_PROCESOS + aleatorio.nextInt(INCREMENTO_PROCESOS);
            for(int i = 0; i < procesos; i++) {
                proceso = new Proceso("Proceso-" + i, TipoProceso.getTipoProceso());
                listaProcesos.add(proceso);
                TimeUnit.SECONDS.sleep(TIEMPO_CREAR_PROCESO);
            }
        
            // Espera antes de enviar la señal de interrupción y espera la finalización
            System.out.println("Hilo(PRINCIPAL) a la espera antes de enviar la señal de INTERRUPCION");
            TimeUnit.SECONDS.sleep(TIEMPO_FINALIZACION);
        } catch(IndexOutOfBoundsException ex) {
            System.out.println("Hilo(PRINCIPAL) ERROR DE CONCURRENCIA en la lista de procesos");
        }
        
        System.out.println("Hilo(PRINCIPAL) solicita la INTERRUPCION de los gestores y espera a su finalización");
        for(Thread hilo : listaHilos)
            hilo.interrupt();
        
        for(Thread hilo : listaHilos)
            hilo.join();
        
        // Presentar resultados
        for(int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("_______ Resultados Gestor " + i + " _____________");
            System.out.println(listaResultados[i]);
            System.out.println("_________________________________________________");
        }
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
