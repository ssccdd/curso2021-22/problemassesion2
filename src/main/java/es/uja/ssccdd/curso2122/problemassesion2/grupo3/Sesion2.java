/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.MIN_COMPONENTES;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.MIN_TIEMPO_COMPONENTE;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.NUM_PROVEEDORES;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.TIEMPO_FINALIZACION;
import es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.TipoComponente;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.VARIACION_COMPONENTES;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.VARIACION_TIEMPO_COMPONENTE;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author UJA
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        Thread[] listaHilos;
        Proveedor proveedor;
        ArrayList<Componente> listaComponentes;
        ArrayList<Ordenador>[] listaResultados;
        Componente componente;
        int numComponentes;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        listaComponentes = new ArrayList();
        listaResultados = new ArrayList[NUM_PROVEEDORES];
        listaHilos = new Thread[NUM_PROVEEDORES];
        for(int i = 0; i < NUM_PROVEEDORES; i++) {
            listaResultados[i] = new ArrayList();
            proveedor = new Proveedor("Proveedor-" + i, listaComponentes, listaResultados[i]);
            listaHilos[i] = new Thread(proveedor);
        }
        
        // Cuerpo de ejecución del hilo principal
        for(Thread hilo : listaHilos)
            hilo.start();
        
        System.out.println("Hilo(PRINCIPAL) comienza la creación de componentes");
        numComponentes = MIN_COMPONENTES + aleatorio.nextInt(VARIACION_COMPONENTES);
        try {
            for(int i = 0; i < numComponentes; i++) {
                componente = new Componente("Componente-" + i, TipoComponente.getTipoComponente());
                listaComponentes.add(componente);
                TimeUnit.SECONDS.sleep(MIN_TIEMPO_COMPONENTE + aleatorio.nextInt(VARIACION_TIEMPO_COMPONENTE));
            }
        
            // Espera antes de enviar la señal de interrupción y espera la finalización
            System.out.println("Hilo(PRINCIPAL) a la espera antes de enviar la señal de INTERRUPCION");
            TimeUnit.SECONDS.sleep(TIEMPO_FINALIZACION);
        } catch(IndexOutOfBoundsException ex) {
            System.out.println("Hilo(PRINCIPAL) ERROR DE CONCURRENCIA en la lista de componentes");
        }
        
        System.out.println("Hilo(PRINCIPAL) solicita la INTERRUPCION de los proveedores y espera a su finalización");
        for(Thread hilo : listaHilos)
            hilo.interrupt();
        
        for(Thread hilo : listaHilos)
            hilo.join();
        
        // Presentar resultados
        for(int i = 0; i < NUM_PROVEEDORES; i++) {
            System.out.println("_______ Resultados Proveedor " + i + " _____________");
            for(Ordenador ordenador : listaResultados[i])
                System.out.println(ordenador);
            System.out.println("_________________________________________________");
        }
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
