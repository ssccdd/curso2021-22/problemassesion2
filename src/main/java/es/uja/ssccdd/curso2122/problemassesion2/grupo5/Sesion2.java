/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo5;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo5.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion2 {

    public static void main(String[] args) {

        // Variables aplicación
        int idReservas = 0;
        int idCoches = 0;
        int idGestores = 0;

        ArrayList<Thread> hilosGestores = new ArrayList<>();
        ArrayList<Coche> cochesPerdidos = new ArrayList<>();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < GESTORES_A_GENERAR; i++) {

            // Inicializamos las reservas
            ArrayList<Reserva> listaReservas = new ArrayList<>();
            for (int j = 0; j < RESERVAS_A_GENERAR; j++) {
                int aleatorioTipo = random.nextInt(VALOR_GENERACION);
                int aleatorioNCoches = random.nextInt(MAXIMO_COCHES_POR_RESERVA) + 1;
                listaReservas.add(new Reserva(idReservas++, aleatorioNCoches, TipoReserva.getTipoReserva(aleatorioTipo)));

            }

            // Inicializamos los coches
            int nCoches = random.nextInt((COCHES_A_GENERAR_MAX + 1) - COCHES_A_GENERAR_MIN) + COCHES_A_GENERAR_MIN;
            ArrayList<Coche> listaCoches = new ArrayList<>();
            for (int j = 0; j < nCoches; j++) {
                int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
                listaCoches.add(new Coche(idCoches++, TipoReserva.getTipoReserva(aleatorioCalidad)));
            }

            // Inicializamos las plataformas
            Gestor gestor = new Gestor(idGestores++, listaReservas, listaCoches, cochesPerdidos);
            Thread thread = new Thread(gestor);
            thread.start();
            hilosGestores.add(thread);

        }

        System.out.println("HILO-Principal Espera a la finalización de los gestores");

        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de las plataformas");

        for (int i = 0; i < GESTORES_A_GENERAR; i++) {
            hilosGestores.get(i).interrupt();
        }

        System.out.println("HILO-Principal Espera a los gestores");

        for (int i = 0; i < GESTORES_A_GENERAR; i++) {
            try {
                hilosGestores.get(i).join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\nLos siguientes " + cochesPerdidos.size() + " coches no han podido ser insertados:");
        for (Coche coche : cochesPerdidos) {
            System.out.println("\t" + coche);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
