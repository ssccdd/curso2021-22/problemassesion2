/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.FIN_EJECUCION;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorProcesos implements Runnable {
    private final String iD;
    private final ArrayList<Proceso> listaProcesos;
    private final RegistroMemoria resultadoEjecucion;

    public GestorProcesos(String iD, ArrayList<Proceso> listaProcesos, RegistroMemoria resultadoEjecucion) {
        this.iD = iD;
        this.listaProcesos = listaProcesos;
        this.resultadoEjecucion = resultadoEjecucion;
    }
    
    @Override
    public void run() {
        boolean finEjecucion;
        
        System.out.println("El HILO(" + iD + ") comienza su ejecución");
        finEjecucion = !FIN_EJECUCION;
        
        try {
            // Hasta su finalización o su interrupción
            do {
                // Podemos no obtener un proceso
                Optional<Proceso> proceso = obtenerProceso();
                
                if( proceso.isPresent() ) {
                    if( !resultadoEjecucion.addProceso(proceso.get()) )
                        listaProcesos.add(proceso.get());
                    else
                        System.out.println("El HILO(" + iD + ") añade el proceso " + proceso +
                                       " a su registro de memoria");
                } else
                    finEjecucion = FIN_EJECUCION;
            } while(!finEjecucion);
            
            System.out.println("El HILO(" + iD + ") ha finalizado su ejecución");
        } catch(InterruptedException ex) {
            System.out.println("El HILO(" + iD + ") ha sido INTERRUMPIDO");
        } catch(IndexOutOfBoundsException ex) {
            System.out.println("El HILO(" + iD + ") ha producido un ERROR DE CONCURRENCIA "
                               + "en la lista de procesos compartidos");
        }
    }

    public String getiD() {
        return iD;
    }
    
    /**
     * Obtenemos un proceso de la lista compartida entre los gestores si está 
     * disponible.
     * Solo se permite la interrupción si no hemos intentado la recuperación
     * de un proceso.
     * @return un objeto opcional que contiene el pedido si está disponible.
     * @throws InterruptedException 
     */
    private Optional<Proceso> obtenerProceso() throws InterruptedException {
        Optional<Proceso> proceso;
        int tiempo;
        
        // Simulamos el tiempo de operación
        tiempo = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
        TimeUnit.SECONDS.sleep(tiempo);
        
        try {
            proceso = Optional.ofNullable(listaProcesos.remove(PRIMERO));
        } catch(IndexOutOfBoundsException ex) {
            // No hay procesos en la lista
            proceso = Optional.empty();
        } 
        
        return proceso;
    }
    
}
