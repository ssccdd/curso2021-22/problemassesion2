/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo5;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo4.Utils.VALOR_GENERACION;
import java.util.ArrayList;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo5.Utils.TipoReserva.*;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo5.Utils.*;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author UJA
 */
public class Gestor implements Runnable {

    private final int iD;
    private final ArrayList<Reserva> reservas;
    private final ArrayList<Coche> cochesPendientes;
    private final ArrayList<Coche> cochesNoAsignados;
    private boolean interrumpido;
    private int cochesProcesados;
    private int siguienteReserva;

    public Gestor(int iD, ArrayList<Reserva> reservas, ArrayList<Coche> cochesPendientes, ArrayList<Coche> cochesNoAsignados) {
        this.iD = iD;
        this.reservas = reservas;
        this.cochesPendientes = cochesPendientes;
        this.cochesNoAsignados = cochesNoAsignados;
        this.interrumpido = false;
        this.cochesProcesados = 0;
        this.siguienteReserva = 0;
    }

    @Override
    public void run() {

        System.out.println("El gestor " + iD + " ha comenzado a preparar las reservas.");

        for (int i = 0; i < cochesPendientes.size(); i++) {

            //Detecta si ha sido interrumpido y el coche a procesar es de gama Premium 
            if (cochesPendientes.get(i).getCalidadCoche().equals(PREMIUM) || !interrumpido) {
                //Se prueba a insertar un coche en las reservas disponibles
                organizarCoche(i);

                //En cualquier caso, se marca como procesado
                cochesProcesados++;

                try {
                    TimeUnit.SECONDS.sleep(Utils.random.nextInt((TIEMPO_ESPERA_MAX + 1) - TIEMPO_ESPERA_MIN) + TIEMPO_ESPERA_MIN);
                } catch (InterruptedException ex) {
                    //Si nos interrupten en la espera del último coche, no lo contamos.
                    if (cochesPendientes.size() != cochesProcesados) {
                        interrumpido = true;
                    }
                }
            }
        }

        System.out.println(this.toString());

    }

    /**
     * Intenta introducir un coche en alguna reserva
     *
     * @param indiceCoche indice del coche a insertar
     * @return true si ha podido ser insertado en alguna reserva
     */
    private boolean organizarCoche(int indiceCoche) {

        Coche coche = cochesPendientes.get(indiceCoche);
        int reservasComprobadas = 0; //Para evitar bucles infinitos
        boolean encolado = false;
        while (!encolado && reservasComprobadas < RESERVAS_A_GENERAR) {// Si no se ha generado una impresora de un tipo, esto evita el bucle infinito

            if (reservas.get(siguienteReserva).addCoche(coche)) {
                encolado = true;
            }

            reservasComprobadas++;
            siguienteReserva = (siguienteReserva + 1) % RESERVAS_A_GENERAR;

        }

        //Si no se ha podido insertar se guarda la incidencia
        if (!encolado) {
            cochesNoAsignados.add(cochesPendientes.get(indiceCoche));
        }

        return encolado;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return procentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * cochesProcesados / cochesPendientes.size();
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nGestor ").append(iD).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\"");
        }

        mensaje.append(" ").append(cochesPendientes.size()).append(" coches disponibles, de los cuales se han procesado el ").append(porcentajeCompletado() * 100).append("%.");

        for (Reserva reserva : reservas) {
            mensaje.append("\n\t").append(reserva.toString());
        }

        if (cochesProcesados < cochesPendientes.size()) {
            mensaje.append("\n\tSe han quedado fuera ").append(cochesPendientes.size() - cochesProcesados).append(" coches por falta de tiempo.");
        }

        return mensaje.toString();
    }

}
