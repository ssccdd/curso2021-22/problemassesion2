/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo4;

import es.uja.ssccdd.curso2122.problemassesion2.grupo4.Utils.TipoRecurso;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo4.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion2 {

    public static void main(String[] args) {
        // Variables aplicación
        int idPlanes = 0;
        int idRecursos = 0;
        int idPlataformas = 0;

        ArrayList<Thread> hilosPlataformas = new ArrayList<>();
        ArrayList<Recurso> recursosPerdidos = new ArrayList<>();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < PLATAFORMAS_A_GENERAR; i++) {

            // Inicializamos los planes
            ArrayList<PlanEstudios> listaPlanes = new ArrayList<>();
            for (int j = 0; j < PLANESTUDIOS_A_GENERAR; j++) {
                listaPlanes.add(new PlanEstudios(idPlanes++));
            }

            // Inicializamos los recursos
            ArrayList<Recurso> listaRecursos = new ArrayList<>();
            for (int j = 0; j < RECURSOS_A_GENERAR; j++) {
                int aleatorioRecurso = random.nextInt(VALOR_GENERACION);
                int aleatorioCreditos = random.nextInt((CREDITOS_MAXIMOS_ASIGNATURA + 1) - CREDITOS_MINIMOS_ASIGNATURA) + CREDITOS_MINIMOS_ASIGNATURA;
                listaRecursos.add(new Recurso(idRecursos++, TipoRecurso.getRecurso(aleatorioRecurso), aleatorioCreditos));
            }

            // Inicializamos las plataformas
            PlataformaAprendizaje plataforma = new PlataformaAprendizaje(idPlataformas++, listaPlanes, listaRecursos, recursosPerdidos);
            Thread thread = new Thread(plataforma);
            thread.start();
            hilosPlataformas.add(thread);

        }

        System.out.println("HILO-Principal Espera a la finalización de las plataformas");

        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de las plataformas");

        for (int i = 0; i < PLATAFORMAS_A_GENERAR; i++) {
            hilosPlataformas.get(i).interrupt();
        }

        System.out.println("HILO-Principal Espera a las plataformas");

        for (int i = 0; i < PLATAFORMAS_A_GENERAR; i++) {
            try {
                hilosPlataformas.get(i).join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\nLos siguientes " + recursosPerdidos.size() + " recursos no han podido ser insertados:");
        for (Recurso p : recursosPerdidos) {
            System.out.println("\t" + p);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
