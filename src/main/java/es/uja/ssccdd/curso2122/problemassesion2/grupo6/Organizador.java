/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo6;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo6.Utils.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author UJA
 */
public class Organizador implements Runnable {

    private final int iD;
    private final ArrayList<Equipo> equipos;
    private final ArrayList<Jugador> jugadoresPendientes;
    private final ArrayList<Jugador> jugadoresNoAsignados;
    private boolean interrumpido;
    private int jugadoresProcesados;

    public Organizador(int iD, ArrayList<Equipo> equipos, ArrayList<Jugador> jugadoresPendientes, ArrayList<Jugador> jugadoresNoAsignados) {
        this.iD = iD;
        this.equipos = equipos;
        this.jugadoresPendientes = jugadoresPendientes;
        this.jugadoresNoAsignados = jugadoresNoAsignados;
        this.interrumpido = false;
        this.jugadoresProcesados = 0;
    }

    @Override
    public void run() {

        System.out.println("El organizador " + iD + " ha comenzado a asignar a los jugadores.");

        for (int i = 0; i < jugadoresPendientes.size(); i++) {

            //Detecta si ha sido interrumpido y se han completado al menos los X equipos
            if (equiposCompletados()<EQUIPOS_MINIMOS_A_COMPLETAR || !interrumpido) {
                //Se prueba a insertar un jugador en los equipos disponibles
                organizarJugador(i);

                //En cualquier caso, se marca como procesado
                jugadoresProcesados++;

                try {
                    TimeUnit.SECONDS.sleep(Utils.random.nextInt((TIEMPO_ESPERA_MAX + 1) - TIEMPO_ESPERA_MIN) + TIEMPO_ESPERA_MIN);
                } catch (InterruptedException ex) {
                    //Si nos interrupten en la espera del último jugador, no lo contamos.
                    if (jugadoresPendientes.size() != jugadoresProcesados) {
                        interrumpido = true;
                    }
                }
            }
        }

        System.out.println(this.toString());

    }

    /**
     * Intenta introducir un juagdor en algun equipo
     *
     * @param indiceJugador indice del jugador a insertar
     * @return true si ha podido ser insertado en algun equipo
     */
    private boolean organizarJugador(int indiceJugador) {

        boolean insertado = false;
        for (int j = 0; j < equipos.size() && !insertado; j++) {
            insertado = equipos.get(j).addJugador(jugadoresPendientes.get(indiceJugador));
        }

        //Si no se ha podido insertar se guarda la incidencia
        if (!insertado) {
            jugadoresNoAsignados.add(jugadoresPendientes.get(indiceJugador));
        }

        return insertado;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return procentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * jugadoresProcesados / jugadoresPendientes.size();
    }

    /**
     * Comprueba los equipos que han sido completados
     *
     * @return número de equipos completos
     */
    private int equiposCompletados() {
        int completos = 0;
        for (Equipo equipo : equipos) {
            if (equipo.isCompleto()) {
                completos++;
            }
        }
        return completos;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nOrganizador ").append(iD).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\"");
        }

        mensaje.append(" ").append(jugadoresPendientes.size()).append(" jugadores disponibles, de los cuales se han procesado el ").append(porcentajeCompletado() * 100).append("%.");
        mensaje.append("\n Equipos disponibles").append(equipos.size()).append(", equipos completados").append(equiposCompletados()).append(".");

        for (Equipo equipo : equipos) {
            mensaje.append("\n\t").append(equipo.toString());
        }

        if (jugadoresProcesados < jugadoresPendientes.size()) {
            mensaje.append("\n\tSe han quedado fuera ").append(jugadoresPendientes.size() - jugadoresProcesados).append(" jugadores por falta de tiempo.");
        }

        return mensaje.toString();
    }

}
