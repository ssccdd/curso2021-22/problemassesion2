/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo4;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo4.Utils.TipoRecurso.ASIGNATURA;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo4.Utils.TIEMPO_ESPERA_MIN;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo4.Utils.TIEMPO_ESPERA_MAX;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class PlataformaAprendizaje implements Runnable {

    private final int iD;
    private final ArrayList<PlanEstudios> planes;
    private final ArrayList<Recurso> recursosPendientes;
    private final ArrayList<Recurso> recursosNoAsignados;
    private boolean interrumpido;
    private int recursosProcesados;

    public PlataformaAprendizaje(int iD, ArrayList<PlanEstudios> planes, ArrayList<Recurso> recursosPendientes, ArrayList<Recurso> recursosNoAsignados) {
        this.iD = iD;
        this.planes = planes;
        this.recursosPendientes = recursosPendientes;
        this.recursosNoAsignados = recursosNoAsignados;
        this.interrumpido = false;
        this.recursosProcesados = 0;
    }

    @Override
    public void run() {

        System.out.println("La plataforma " + iD + " ha comenzado a asignar los recursos.");

        for (int i = 0; i < recursosPendientes.size(); i++) {

            //Detecta si ha sido interrumpido y si el recurso a procesar es de tipo asignatura 
            if (recursosPendientes.get(i).getTipo().equals(ASIGNATURA) || !interrumpido) {
                //Se prueba a insertar un recursos en los planes disponibles
                organizarRecurso(i);

                //En cualquier caso, se marca como procesado
                recursosProcesados++;

                try {
                    TimeUnit.SECONDS.sleep(Utils.random.nextInt((TIEMPO_ESPERA_MAX+1) - TIEMPO_ESPERA_MIN) + TIEMPO_ESPERA_MIN);
                } catch (InterruptedException ex) {
                    //Si nos interrupten en la espera del último recurso, no lo contamos.
                    if (recursosPendientes.size() != recursosProcesados) {
                        interrumpido = true;
                    }
                }
            }
        }

        System.out.println(this.toString());

    }

    /**
     * Intenta introducir un recurso en algún plan
     *
     * @param indiceRecurso indice del recurso a insertar
     * @return true si ha podido ser insertado en algún plan
     */
    private boolean organizarRecurso(int indiceRecurso) {

        boolean insertado = false;
        for (int j = 0; j < planes.size() && !insertado; j++) {
            insertado = planes.get(j).addRecurso(recursosPendientes.get(indiceRecurso));
        }

        //Si no se ha podido insertar se guarda la incidencia
        if (!insertado) {
            recursosNoAsignados.add(recursosPendientes.get(indiceRecurso));
        }

        return insertado;

    }

    /**
     * Calcula el porcentaje de trabajo realizado
     *
     * @return procentaje de trabajo realizado
     */
    private float porcentajeCompletado() {
        return 1.0f * recursosProcesados / recursosPendientes.size();
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nPlataforma ").append(iD).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\"");
        }

        mensaje.append(" ").append(recursosPendientes.size()).append(" recursos disponibles, de los cuales se han procesado el ").append(porcentajeCompletado() * 100).append("%.");

        for (PlanEstudios plan : planes) {
            mensaje.append("\n\t").append(plan.toString());
        }

        if (recursosProcesados < recursosPendientes.size()) {
            mensaje.append("\n\tSe han quedado fuera ").append(recursosPendientes.size() - recursosProcesados).append(" recursos por falta de tiempo.");
        }

        return mensaje.toString();
    }

}
