/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion2.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.FIN_EJECUCION;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2122.problemassesion2.grupo3.Constantes.MAX_INTENTOS;
import java.util.Optional;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final String iD;
    private final ArrayList<Componente> listaComponentes;
    private final ArrayList<Ordenador> resultadoEjecucion;
    private int numIntentos;

    public Proveedor(String iD, ArrayList<Componente> listaComponentes, ArrayList<Ordenador> resultadoEjecucion) {
        this.iD = iD;
        this.listaComponentes = listaComponentes;
        this.resultadoEjecucion = resultadoEjecucion;
        this.numIntentos =0;
    }
    
    @Override
    public void run() {
        boolean finEjecucion;
        
        System.out.println("El HILO(" + iD + ") comienza su ejecución");
        finEjecucion = !FIN_EJECUCION;
        
        try {
            // Hasta su finalización o su interrupción
            do {
                // Puede que no obtengamos un componente
                Optional<Componente> componente = obtenerComponente();
                
                if( componente.isPresent() ) { 
                    // Comprobamos que tenemos un componente
                    if( !asignarComponente(componente.get()) )
                        nuevoOrdenador(componente.get());
                } else {
                    numIntentos++;
                    if( numIntentos == MAX_INTENTOS )
                        finEjecucion = FIN_EJECUCION;
                }  
            } while(!finEjecucion);
            
            System.out.println("El HILO(" + iD + ") ha finalizado su ejecución");
        } catch(InterruptedException ex) {
            System.out.println("El HILO(" + iD + ") ha sido INTERRUMPIDO");
        }
        
    }

    public String getiD() {
        return iD;
    }
    
    /**
     * Optiene un componente si está presente en la lista de componentes 
     * compartida entre los proveedores.
     * Solo se permite la interrupción si no hemos obrenido un componente.
     * @return el componente si se encuentra presente en la lista
     * @throws InterruptedException 
     */
    private Optional<Componente> obtenerComponente() throws InterruptedException {
        Optional<Componente> componente;
        int tiempo;
        
        // Simulamos el tiempo de operación
        tiempo = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
        TimeUnit.SECONDS.sleep(tiempo);
        
        try {
            componente = Optional.ofNullable(listaComponentes.remove(PRIMERO));
        } catch(IndexOutOfBoundsException ex) {
            // No hay componente
            componente = Optional.empty();
        }
        
        return componente;
    }
    
    /**
     * Asigna un componente a la lista de ordenadores disponibles si es posible
     * @param componente
     * @return Si se ha podido añadir el componente a un ordenador
     */
    private boolean asignarComponente(Componente componente) {
        boolean asignado = !ASIGNADO;
        Iterator it;
        
        // Asignamos el componente al primer ordenador posible
        it = resultadoEjecucion.iterator();
        while( it.hasNext() && !asignado ) { 
            Ordenador ordenador = (Ordenador) it.next();
            if( ordenador.addComponente(componente) ) 
                asignado = ASIGNADO;    
        }
                
        return asignado;
    }
    
    /**
     * Crea un nuevo ordenador donde añadir el componente
     * @param componente 
     */
    private void nuevoOrdenador(Componente componente) {
        Ordenador ordenador;
        
        ordenador = new Ordenador("Ordenador-" + resultadoEjecucion.size());
        ordenador.addComponente(componente);
        resultadoEjecucion.add(ordenador);
        
        System.out.println("El HILO(" + iD + ") ha creado un ordenador para " + componente);
    }
}
